﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HapticFeedback : MonoBehaviour {

    public GameObject ControllerLeft;
    public GameObject ControllerRight;

    public float strength = 1.0f;       // The vibration strength

    private SteamVR_Controller.Device deviceLeft;
    private SteamVR_Controller.Device deviceRight;

    private float currPositionLeft, currPositionRight;
    private float prevPositionLeft, prevPositionRight;


    // Use this for initialization
    void Start() {
        
        // Obtain current positions

        currPositionLeft = ControllerLeft.transform.localPosition.y;
        prevPositionRight = ControllerRight.transform.localPosition.y;

        prevPositionLeft = ControllerLeft.transform.localPosition.y;
        prevPositionRight = ControllerRight.transform.localPosition.y;
    }

    // Update is called once per frame
    void Update() {


        // Check if both controllers are detected, otherwise do nothing.
        if (ControllerLeft.activeInHierarchy && ControllerRight.activeInHierarchy) {

            int indexLeft = (int)ControllerLeft.GetComponent<SteamVR_TrackedObject>().index;
            deviceLeft = SteamVR_Controller.Input(indexLeft);

            int indexRight = (int)ControllerRight.GetComponent<SteamVR_TrackedObject>().index;
            deviceRight = SteamVR_Controller.Input(indexRight);



        } else {
            //Debug.Log(this.name +  ": Controller not initalized");
            return;
        }


        // Obtain current positions

        prevPositionLeft = currPositionLeft;
        prevPositionRight = currPositionRight;

        currPositionLeft = ControllerLeft.transform.localPosition.y;
        currPositionRight = ControllerRight.transform.localPosition.y;

    }


    void FixedUpdate() {

        // Trigger feedback here, so it stays consistent between systems.

        TriggerFeedback(deviceLeft, prevPositionLeft, currPositionLeft);
        TriggerFeedback(deviceRight, prevPositionRight, currPositionRight);

    }

    /// <summary>
    /// Trigger Haptic Feedback to <paramref name="device"/> according to its vertical movement speed.
    /// </summary>
    /// <param name="device"></param>
    /// <param name="prevPos"></param>
    /// <param name="currPos"></param>
    private void TriggerFeedback(SteamVR_Controller.Device device, float prevPos, float currPos) {

        // Calculate movement speed
        float speed = Mathf.Abs(currPos - prevPos) / Time.deltaTime;
        //Debug.Log("speed: " + speed);
        speed = Mathf.Clamp(speed, 0, 1.0f);

        float delta = speed;// / 3.0f;

        if (device != null && delta > 0.03f) {
            device.TriggerHapticPulse((ushort)(delta * strength * 3999));
        }
    }


    /// <summary>
    /// This function sends a single pulse to both controllers.
    /// </summary>
    /// <param name="strength"></param>     The strength of the vibration, between 0 and 1.
    /// <param name="duration"></param>     The total duration of the haptic feedback.
    /// <param name="pulseInterval"></param> The pause between pulses.
    public void SendHapticPulse(float strength, float duration, float pulseInterval) {
        if (!enabled) {
            return;
        }

        StartCoroutine(HapticPulse(duration, strength, pulseInterval));
    }

    private IEnumerator HapticPulse(float duration, float strength, float pulseInterval) {
        if (pulseInterval <= 0) {
            yield break;
        }

        while (duration > 0) {
            
            deviceLeft.TriggerHapticPulse((ushort)(strength * 3999));
            deviceRight.TriggerHapticPulse((ushort)(strength * 3999));
            yield return new WaitForSeconds(pulseInterval);
            duration -= pulseInterval;
        }
    }

}
