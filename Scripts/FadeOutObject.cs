﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FadeOutObject : MonoBehaviour {

    public Collider otherCollider;

    public float m_FadeDuration = 0.5f;       // How long it takes for the arrows to appear and disappear.

    private float m_StartAlpha;                 // The alpha the pattern has at the beginning.
    private float m_CurrentAlpha = 1.0f;        // The alpha the pattern currently has.
    private float m_TargetAlpha;                // The alpha the pattern is fading towards.
    private float m_FadeSpeed;                  // How much the alpha should change per second (calculated from the fade duration).
    private float m_ShowDistance = 1000;        // How close the player must be for the pattern to appear.
    private Collider collider;

    private const string k_MaterialPropertyName = "_Alpha";     // The name of the alpha property on the shader being used to fade the pattern.

    // Use this for initialization
    void Start () {
        collider = GetComponent<Collider>();

        // Speed is distance (zero alpha to one alpha) divided by time (duration).
        m_FadeSpeed = 1f / m_FadeDuration;

        // Get max alpha value
        m_StartAlpha = GetComponent<Renderer>().material.GetFloat(k_MaterialPropertyName);
    }
	
	// Update is called once per frame
	void Update () {

        Vector3 closestPoint = collider.ClosestPointOnBounds(otherCollider.transform.position);
        float distance = Vector3.Distance(closestPoint, otherCollider.transform.position);

        //Debug.Log("distance to wall: " + distance);

        // If the distance is smaller than the angle at which the arrows are shown, their target alpha is one otherwise it is zero.
        m_TargetAlpha = distance < m_ShowDistance ? 0f : m_StartAlpha;

        // Increment the current alpha value towards the now chosen target alpha and the calculated speed.
        m_CurrentAlpha = Mathf.MoveTowards(m_CurrentAlpha, m_TargetAlpha, m_FadeSpeed * Time.deltaTime);

        // Go through all the arrow renderers and set the given property of their material to the current alpha.
        GetComponent<Renderer>().material.SetFloat(k_MaterialPropertyName, m_CurrentAlpha);
		
	}



    void OnDrawGizmosSelected()
    {
        UnityEditor.Handles.color = Color.yellow;
        UnityEditor.Handles.DrawWireDisc(this.transform.position, Vector3.down, m_ShowDistance);
    }


}


    



