﻿using UnityEngine;
using System.Collections;


/// <summary>
///     Controls a skydiving and parachute flight using Unity's built-in physics.
///     This is based on <see cref="FlyingControl"/> by (?)
/// </summary>
public class ParachuteFlightControl : MonoBehaviour {

    public static readonly int STANDING = 0;
    public static readonly int FALLING = 1;
    public static readonly int GLIDING = 2;
    public static readonly int LANDING = 3;
    public static readonly int LANDED = 4;

    public static readonly float MAX_SPEED = 50f;

    public float parachuteOpenHeight = 200.0f;

    public GameObject[] targets;

    public GameObject speedEffect;
    public GameObject effects;

    public DayNightCycle dn_cycle;

    public AudioSource windSound;
    public AudioSource parachuteSound;
    public GameObject flightMusic;

    private Collider trajectory_radar;
    private Rigidbody rb;

    private Ray trajectory;

    public int state = STANDING;

    public GameObject _screenPlayPlayer;
    private ScreenPlayPlayer _screenPlayPlayerScript;


    // Use this for initialization
    void Start() {
        trajectory_radar = this.GetComponent<BoxCollider>();
        rb = this.GetComponent<Rigidbody>();

        _screenPlayPlayerScript = _screenPlayPlayer.GetComponent<ScreenPlayPlayer>();
    }

    // Update is called once per frame
    void Update() {
        float distToTarget = GetDistanceToNextTarget();

        //Wind resistance keeps the velocity from surpassing 50 m/s
        if (Mathf.Abs(rb.velocity.y) > MAX_SPEED) {
            rb.velocity += Vector3.up * 10.0f;
        }

        //Debug.Log ( rb.velocity );

        //Standing in plane
        if (state == STANDING) {

            if (Input.GetButtonDown("Jump")) {
                rb.AddRelativeForce((Vector3.forward * 3.0f + Vector3.up * 2.0f) * 200.0f, ForceMode.Impulse);
                StartCoroutine(nextState());
                _screenPlayPlayerScript.play();
            }
            //Falling
        }
        else if (state == FALLING) {
            RaycastHit hit;
            if (trajectory_radar.Raycast(trajectory, out hit, 10000.0f)) {

                if (transform.position.y > parachuteOpenHeight) {
                    //Constant force pulling player towards the ideal trajectory
                    //Vector3 dir = hit.point - transform.position;
                    //rb.AddRelativeForce( dir * 200.0f *Time.deltaTime, ForceMode.Acceleration );
                   // float speed = MAX_SPEED;
                   // Vector3 direction = (targets[state].transform.position - transform.position).normalized;
                    //Debug.Log (distToTarget);
                    
                    //Vector3 targetPosition2d = targets[state].transform.position;
                    //targetPosition2d.y = transform.position.y;

                    //Vector3 direction = (targets[state].transform.position - transform.position).normalized;
                    //rb.velocity = direction * speed;
                }
                else {
                    StartCoroutine(nextState());
                }
            }
            //Parachute opened we are gliding towards landing point
        }
        else if (state == GLIDING) {

            if (distToTarget > 10) {
                if (distToTarget < 25000) {

                }
            }
            else {
                StartCoroutine(nextState());
            }
            //Landing point reached we are slowing down and touching ground
        }
        else if (state == LANDING) {
            if (distToTarget > 10) {
                float speed = rb.velocity.magnitude;
                Vector3 direction = (targets[state].transform.position - transform.position).normalized;
                rb.velocity = direction * speed;
            }
            else {
                StartCoroutine(nextState());
            }
        }

    }

    /*Switches from one state to the next
	 * Each transition from one state to the next can be cause additional actions defined here
	 * Additional actions:
	 *  -The initial jump
	 * 	-Parachute opens and slows the player down
	 * 	-Recalculating the ideal falling trajectory
	*/
    private IEnumerator nextState() {

        if (state == STANDING) {

            //Debug.Log("Free falling...");
            yield return new WaitForSeconds(2);
            state = FALLING;
            //Look towards next target
            //speedEffect.transform.LookAt(this.targets[state].transform.position);
            //speedEffect.transform.Rotate(Vector3.up * 180.0f);
            speedEffect.GetComponent<ParticleSystem>().Play();
            dn_cycle.play = true;
            windSound.Play();

            //The player is pulled towards the ideal trajectory
            trajectory = new Ray(transform.position, (targets[state].transform.position - transform.position));

            //Debug.Log ("Now stabilizing trajectory.");

            effects.transform.LookAt(targets[state].transform.position);

        }
        else if (state == FALLING) {

            //Debug.Log ("Opening parachute...");

            //No more free falling
            rb.useGravity = false;

            //Deactivate SpeedEffect particle system
            speedEffect.GetComponent<ParticleSystem>().Stop();
            speedEffect.SetActive(false);
            windSound.volume = 0.1f;
            parachuteSound.Play();
            state = GLIDING;
            effects.transform.LookAt(targets[state].transform.position);

            float speed = rb.velocity.magnitude;
            Vector3 direction = (targets[state].transform.position - transform.position).normalized;
            rb.velocity = direction * speed * 12.0f;
            //Debug.Log ("Now gliding towards landing point.");

        }
        else if (state == GLIDING) {

            //Debug.Log ("Landing...");
            state = LANDING;
            windSound.volume = 0.0f;
            effects.transform.LookAt(targets[state].transform.position);

        }
        else if (state == LANDING) {

            Debug.Log("Landed.");
            rb.velocity = Vector3.zero;
            rb.useGravity = true;
            state = LANDED;
            effects.transform.LookAt(Vector3.forward);

            flightMusic.GetComponent<MusicFadeOut>().fadeout = true;

        }
    }

    public float GetDistanceToNextTarget() {
        float distToTarget = 0.0f;
        if (state != LANDED) {
            distToTarget = (this.targets[state].transform.position - this.transform.position).magnitude;
        }
        return distToTarget;
    }
}
