﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This script allows the screenplay to be started automatically when the user
/// jumps from the platform. A combination of speed and position is used to detect
/// a jump.
/// </summary>
[RequireComponent(typeof(FlyingControl))]
public class JumpStart_Vive : MonoBehaviour {

    public GameObject Headset;
    public GameObject ControllerLeft;
    public GameObject ControllerRight;

    public float StartDistance = 0.5f;

    public bool RequireControllers = true;

    private bool _initialized = false;
    private bool _startPossible = false;
    private Vector3 _initPosition;

    

    private Vector3 prevHMDPosition;

    // Use this for initialization
    void Start () {
        prevHMDPosition = Headset.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        // Daten für Dev-Cube:
        // autostart bei z <= 0.3f
        // und speed >= 2

	    if (Input.GetButtonDown("Action")) {
            // Enable/Disable if start is possible
            // Should be synced with the lights
	        _startPossible = !_startPossible;
            _initPosition = Headset.transform.localPosition;

            if (_startPossible && _initialized)
                Debug.Log(this.name + ": Jump start activated, the jump will be started when the distance to the current position is >= " + StartDistance + ".");
            else
                Debug.Log(this.name + ": Jump start disabled or not yet initialized.");
        }

	    if (!_initialized) {
            // Check if Headset & both controllers are there
            if (Headset.activeInHierarchy &&
                    (!RequireControllers ||
                    (ControllerLeft.activeInHierarchy && ControllerRight.activeInHierarchy)))
            {
                _initialized = true;
                Debug.Log(this.name + ": Jump start initialized, press 'Action' to enable the user to jump.");
            }
        }
        


        if (_initialized && _startPossible) {

            float distance = Vector3.Distance(_initPosition, Headset.transform.localPosition);
            //Debug.Log("distance: " + distance);
            if (distance >= StartDistance) { 

                // Start the Jump
                GetComponent<FlyingControl>().StartJump();

                // Disable this Component, to prevent firing again
                this.enabled = false;
            }

        }
        




        prevHMDPosition = Headset.transform.localPosition;
		
	}
}
