﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This provides a hacky fix for the floating point accuracy problem in Unity.
/// 
/// The Problem:
/// The further away the player is from the origin, the less accurate his movements become.
/// This is due to the fact that Unity's coordinate system uses 7 significant digits for transform
/// positions. The result is very choopy movements, avatar animations are basically not possible
/// on the plane. More info: http://davenewson.com/posts/2013/unity-coordinates-and-scales.html
/// 
/// The solution:
/// The player is repeatedly moved to the origin of the scene. Along with that, everything else has to
/// be moved as well. Therefore, every non-player object has to be a child of the World gameobject.
/// As far as I could test, no significant problems with physics (or otherwise) have occured due
/// to this fix.
/// </summary>
/// <author>Felix Hochgruber</author>
public class InverseMovement : MonoBehaviour {

	public GameObject world;

	// Use this for initialization
	void Start () {
		StartCoroutine (SetOrigin());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator SetOrigin() {
		while(Application.isPlaying) {

			world.transform.position -= this.transform.position;
			this.transform.position = Vector3.zero;

            yield return new WaitForSeconds(5.0f);

        }

	}
}
