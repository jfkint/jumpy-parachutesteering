﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This Script disables a GameObject when a Collider is entered into the Collider attached to the same GameObject as this script.
/// It is used to hide the city roofs when the player is passing through the Kuppelsaal.
/// </summary>
///

[RequireComponent(typeof(Collider))]
public class WallHack : MonoBehaviour {

    public Collider player;
    public GameObject walls;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other == player)
        {
            walls.SetActive(false);
        }
    }
}
