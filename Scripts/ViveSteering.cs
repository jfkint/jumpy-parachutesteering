﻿using UnityEngine;
using System.Collections;

public class ViveSteering : MonoBehaviour {
    private float _velocity = 0.0f;
    private float _direction;

    private bool _fixingRotation = false;
    private float _rotationFixSpeed = 1.0f;

    private FlyingControl _flyingControl;

    //public GameObject IdealPosition;
    public GameObject MovingChild;
    public GameObject ControllerLeft;
    public GameObject ControllerRight;

    public float SkydivingMaxVelocity = 100.0f;
    public float SkydivingSensitivity = 5.0f;
    public float SkydivingDrag = 2.0f;

    public float ParachuteMaxVelocity = 40.0f;
    public float ParachuteSensitivity = 1.0f;
    public float ParachuteDrag = 0.3f;

    public float MaxDistanceToIdeal = 500.0f;
    //public float MaxGlidingAngle = 30.0f;
    public float MaxControllerTilt = 25.0f;

    // Use this for initialization
    void Start() {
        _flyingControl = GetComponent<FlyingControl>();
    }

    // Update is called once per frame
    void Update() {

        // Get Input (debug)


        if (_fixingRotation) {
            // Slowly rotate towards target
            Vector3 targetPos = _flyingControl.targets[FlyingControl.GLIDING].transform.position;

            // Exclude y difference, so we don't get pitch rotation
            targetPos.y = this.transform.position.y;

            // Calculate the needed rotation
            Quaternion targetRotation = Quaternion.LookRotation(targetPos - transform.position);

            // Interpolate!
            Quaternion interpolatedRotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * _rotationFixSpeed);
            GetComponent<Rigidbody>().MoveRotation(interpolatedRotation);
        }
        else {

            float xMotion = GetInputMotion();


            if (_flyingControl.state == FlyingControl.FALLING) {
                // SKYDIVING MODE

                _velocity = CalculateVelocity(_flyingControl.state, xMotion, _velocity, SkydivingMaxVelocity,
                    SkydivingSensitivity, SkydivingDrag);

                MovingChild.transform.Translate(Vector3.right * Time.deltaTime * _velocity);


            }
            else if (_flyingControl.state == FlyingControl.GLIDING) {
                // PARACHUTE MODE

                Rigidbody rb = GetComponent<Rigidbody>();

                // Re-set the child to the parent's position
                // This needs to be done when switching from skydiving to parachuting
                if (MovingChild.transform.localPosition != Vector3.zero) {
                    rb.MovePosition(MovingChild.transform.position);
                    MovingChild.transform.localPosition = Vector3.zero;
                }

                _velocity = CalculateVelocity(_flyingControl.state, xMotion, _velocity, ParachuteMaxVelocity,
                    ParachuteSensitivity, ParachuteDrag);

                // Rotate the player back into upright position

                Quaternion rotation = rb.rotation;
                rotation *= Quaternion.Euler(Vector3.up * Time.deltaTime * _velocity);

                rb.MoveRotation(rotation);
            }

        }


        

        // re set position of parent/child
        
    }


    /// <summary>
    /// Obtain the steering input from the Vive controllers (alternatively: the Q and E keys).
    /// </summary>
    /// <returns>The input in [-1, 1], where -1 is full left and +1 is full right.</returns>
    private float GetInputMotion() {
        float xMotion = 0.0f;

        _direction = 0;
        if (Input.GetKey(KeyCode.Q)) {
            xMotion += -1.0f;
        }

        if (Input.GetKey(KeyCode.E)) {
            xMotion += 1.0f;
        }


        // Get Vive Data
        Vector3 delta = ControllerRight.transform.localPosition - ControllerLeft.transform.localPosition;

        if (delta != Vector3.zero) {
            Quaternion look = Quaternion.LookRotation(delta);
            float angle = look.eulerAngles.x;

            // map from [0, 360] to [-180, 180]
            angle = (angle > 180) ? angle - 360 : angle;
            //Debug.Log("Angle: " + angle);

            // max angle is (MaxControllerTilt) deg, map to [-1, 1]
            xMotion = Mathf.Clamp(angle, -MaxControllerTilt, MaxControllerTilt) / MaxControllerTilt;
        }


        return xMotion;
    }


    /// <summary>
    /// Calculates the left/right motion during falling, and the steering speed during gliding.
    /// </summary>
    /// <param name="state"></param>
    /// <param name="xMotion"></param>
    /// <param name="currentVelocity"></param>
    /// <param name="maxVelocity"></param>
    /// <param name="sensitivity"></param>
    /// <param name="drag"></param>
    /// <returns></returns>
    private float CalculateVelocity(int state, float xMotion, float currentVelocity, float maxVelocity, float sensitivity, float drag) {

        // Apply sensitivity
        float motionAmount = xMotion*sensitivity;

        currentVelocity += motionAmount;

        currentVelocity = Mathf.Clamp(currentVelocity, -maxVelocity, maxVelocity);

        // Clamp values
        if (currentVelocity < 0) {
            currentVelocity = Mathf.Min(currentVelocity + drag, 0);
        }
        else {
            currentVelocity = Mathf.Max(currentVelocity - drag, 0);
        }

        //Debug.Log("speed: " + _velocity);

        // begin slowing down at 80%
        float slowdownBegin = MaxDistanceToIdeal * 0.8f;

        // calculate current position (if distance greater than 80%)

        float distanceToIdeal = MovingChild.transform.position.x - transform.position.x;
        if (Mathf.Abs(distanceToIdeal) > slowdownBegin) {
            float t = (Mathf.Abs(distanceToIdeal) - slowdownBegin) / (MaxDistanceToIdeal - slowdownBegin);

            float interpolatedSpeed = Mathf.Lerp(maxVelocity, 0, t);
            //Debug.Log("interp. speed: " + interpolatedSpeed);

            bool leftOfParent = distanceToIdeal < 0;
            if (currentVelocity < 0 && leftOfParent) {
                // we are left of the parent, and want to go left
                currentVelocity = Mathf.Max(currentVelocity, -interpolatedSpeed);
            }
            else if (currentVelocity > 0 && !leftOfParent) {
                // we are right of the parent, and want to go right
                currentVelocity = Mathf.Min(currentVelocity, interpolatedSpeed);
            }

            // if we want to go in the other direction, don't restrict the speed!

        }

        return currentVelocity;
    }


    /// <summary>
    /// Rotates the player back towards the goal.
    /// </summary>
    /// <returns></returns>
    private IEnumerator FixRotation() {

        Debug.Log("Crossed the bounds, fixing rotation...");

        _fixingRotation = true;
        // Disable steering for (n) seconds
        yield return new WaitForSeconds(3);

        // Enable again
        _fixingRotation = false;

    }


    void OnTriggerEnter(Collider other) {

        // When a collision with a wall occurs, rotate the player towards the goal.
        if (other.tag == "WorldBorder") {
            StartCoroutine(FixRotation());
        }
    }
}