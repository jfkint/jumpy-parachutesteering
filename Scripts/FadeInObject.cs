﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInObject : MonoBehaviour {

    public Transform player;                    // Reference to the player to determine which way he is facing.

    public float m_FadeDuration = 0.5f;       // How long it takes for the arrows to appear and disappear.
    public float m_ShowDistance = 1.0f;        // How near to the wall the player must be for the pattern to appear.
    private float m_CurrentAlpha;               // The alpha the pattern currently has.
    private float m_TargetAlpha;                // The alpha the pattern is fading towards.
    private float m_FadeSpeed;                  // How much the alpha should change per second (calculated from the fade duration).

    private const string k_MaterialPropertyName = "_Alpha";     // The name of the alpha property on the shader being used to fade the pattern.

    private Collider coll;

    // Use this for initialization
    void Start () {
        coll = GetComponent<Collider>();

        // Speed is distance (zero alpha to one alpha) divided by time (duration).
        m_FadeSpeed = 1f / m_FadeDuration;
    }
	
	// Update is called once per frame
	void Update () {


        Vector3 closestPoint = coll.ClosestPointOnBounds(player.position);
        float distance = Vector3.Distance(closestPoint, player.position);

        //Debug.Log("distance to wall: " + distance);

        // If the distance is smaller than the angle at which the arrows are shown, their target alpha is one otherwise it is zero.
        m_TargetAlpha = distance < m_ShowDistance? 1f : 0f;

        // Increment the current alpha value towards the now chosen target alpha and the calculated speed.
        m_CurrentAlpha = Mathf.MoveTowards(m_CurrentAlpha, m_TargetAlpha, m_FadeSpeed * Time.deltaTime);

        // Go through all the arrow renderers and set the given property of their material to the current alpha.
       GetComponent<Renderer>().material.SetFloat(k_MaterialPropertyName, m_CurrentAlpha);
    }
}
